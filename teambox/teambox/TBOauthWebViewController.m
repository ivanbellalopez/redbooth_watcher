//
//  TBOauthWebViewController.m
//  teambox
//
//  Created by Ivan Bella López on 5/31/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import "TBOauthWebViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIView+TBAdditions.h"
#import "TBConstants.h"
#import "AFJSONRequestOperation.h"
#import "TBAppController.h"
#import "TBNetwork.h"

#define _kTBButtonMargin    13.0


@interface TBOauthWebViewController ()

@property (nonatomic, strong) UIWebView     *webView;
@property (nonatomic, strong) UIView        *headerView;
@property (nonatomic, strong) UIImageView   *headerImageView;
@property (nonatomic, strong) UIButton      *cancelButton;
@property (nonatomic, strong) UIButton      *refreshButton;
@property (nonatomic, strong) UIActivityIndicatorView *spinner;

@property (nonatomic, strong) NSString *returnedCode;

@end

@implementation TBOauthWebViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 60.0 - 6.0, self.view.frame.size.width, self.view.frame.size.height - self.headerView.frame.size.height - 20.0)];
    self.webView.delegate = self;
	self.navigationController.navigationBarHidden = NO;
    
    [self _initTeamboxOAuth];
    
    [self.view addSubview:self.webView];
    [self _setUpHeaderView];
    [self _setUpSpinner];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)_setUpHeaderView
{
    UIImage *headerImage = [UIImage imageNamed:@"header_webview.png"];
    self.headerView = [[UIView alloc] initWithFrame:(CGRect){{0, 0}, headerImage.size}];
    self.headerImageView = [[UIImageView alloc] initWithFrame:(CGRect){{0, 0}, headerImage.size}];
    self.headerImageView.image = headerImage;
    [self.headerView addSubview:self.headerImageView];
    
    UIImage *cancelImage = [UIImage imageNamed:@"btton_webview.png"];
    self.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.cancelButton setBackgroundImage:cancelImage forState:UIControlStateNormal];
    
    self.cancelButton.frame = (CGRect){{_kTBButtonMargin, _kTBButtonMargin}, cancelImage.size};
    [self.cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [self.cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.cancelButton.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [self.cancelButton addTarget:self action:@selector(_dismissView) forControlEvents:UIControlEventTouchUpInside];
    [self.headerView addSubview:self.cancelButton];
    
    
    UIImage *refreshImage = [UIImage imageNamed:@"refresh.png"];
    self.refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.refreshButton setBackgroundImage:refreshImage forState:UIControlStateNormal];
    
    self.refreshButton.frame = (CGRect){{self.headerView.frame.size.width - refreshImage.size.width - _kTBButtonMargin, 0.0}, refreshImage.size};
    [self.refreshButton tbCenterVerticallyInView:self.headerView];
    [self.refreshButton addTarget:self action:@selector(_reloadPage) forControlEvents:UIControlEventTouchUpInside];
    [self.headerView addSubview:self.refreshButton];
    
    [self.view addSubview:self.headerView];
}


- (void)_setUpSpinner
{
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.spinner.frame = CGRectMake(0, 0, 70.0, 70.0);
    self.spinner.layer.backgroundColor = [UIColor darkGrayColor].CGColor;
    self.spinner.layer.cornerRadius = 5.0;
    [self.spinner tbCenterInView:self.view];
    [self.view addSubview:self.spinner];
}

#pragma mark - Actions

- (void)_dismissView
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)_reloadPage
{
    if (![self.spinner isAnimating]) {
        [self.spinner startAnimating];
    }
    
    [self.webView stopLoading];
    [self.webView reload];
}


- (void)_initTeamboxOAuth
{
    NSString *path = [NSString stringWithFormat:@"%@/oauth/authorize?client_id=%@&redirect_uri=http://www.3lokoj.com&response_type=code&scope=read_projects+write_projects+offline_access", kTB_Base_URL, TBAppKey];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:path]];
    [self.webView loadRequest:request];
}



#pragma mark - WebView delegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    [self.spinner startAnimating];
    if (request) {
        return YES;
    }
    return NO;
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.spinner stopAnimating];
    NSString *URLString = [[self.webView.request URL] absoluteString];
    
    if ([[[self.webView.request URL] host] isEqualToString:@"www.3lokoj.com"]) {
        self.returnedCode = [[[[URLString componentsSeparatedByString:@"&"] objectAtIndex:0]componentsSeparatedByString:@"=" ] lastObject];
        [[TBAppController sharedClient].defaults setObject:self.returnedCode forKey:kTB_Return_Code];
        [[TBNetwork sharedClient] getAccessToken];
        
        [TBNetwork sharedClient].successBlock = ^{
            [self _dismissView];
        };
        
        [TBNetwork sharedClient].failureBlock = ^{
            //
        };
        
        
    }
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.spinner stopAnimating];
    UIAlertView *aV = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [aV show];
}

@end

