//
//  TBProjectsViewController.h
//  teambox
//
//  Created by Ivan Bella López on 6/3/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TBViewController.h"

@interface TBProjectsViewController : TBViewController <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSFetchedResultsController *getProjectsFetchedResultsController;

@end
