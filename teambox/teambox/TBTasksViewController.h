//
//  TBTasksViewController.h
//  teambox
//
//  Created by Ivan Bella López on 6/9/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import "TBViewController.h"
#import "Project.h"


@interface TBTasksViewController : TBViewController <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSFetchedResultsController *getTasksFetchedResultsController;

- (id) initWithProject:(Project *)project;

@end
