//
//  TBDataManager.m
//  teambox
//
//  Created by Ivan Bella López on 6/6/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import "TBDataManager.h"
#import "Project.h"
#import "NSManagedObjectContext-EasyFetch.h"
#import "Organization.h"
#import "TaskList.h"
#import "Task.h"

@interface TBDataManager ()

@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@end

@implementation TBDataManager


+ (TBDataManager *) sharedManager
{
    static TBDataManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
        
    });
    return sharedManager;
}


#pragma mark - init

- (id)init
{
    if (self = [super init]) {
        self.dateFormatter = [[NSDateFormatter alloc] init];
        [self.dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss ZZZ"];
    }
    
    return self;
}


#pragma mark - Data

- (void)save:(NSString *)object withDictionary:(NSDictionary *)dictionary
{
    NSManagedObject *managedObject = [NSEntityDescription
                                      insertNewObjectForEntityForName:object
                                      inManagedObjectContext:self.managedObjectContext];
    
    NSDictionary *attributes = [[managedObject entity] attributesByName];
    
    
    for (NSString *attribute in attributes) {
        
        id value = [dictionary objectForKey:attribute];
        
        if (value == nil) {
            continue;
        }
        
        if ([attribute isEqualToString:@"id"]) {
            
            //check if object exists already
            NSArray *managedObjectArray = [self.managedObjectContext fetchObjectsForEntityName:object
                                                                           predicateWithFormat:[NSString stringWithFormat:@"id == %@", value]];
            
            if (managedObjectArray && [managedObjectArray count] > 0) {
                managedObject = managedObjectArray[0];
            }
            
            if ([object isEqualToString:@"Organization"]) {
                NSArray *projects = [self.managedObjectContext fetchObjectsForEntityName:@"Project"
                                                                     predicateWithFormat:[NSString stringWithFormat:@"organization_id == %@", value]];
                for (Project *project in projects) {
                    [(Organization*)managedObject addProjectObject:project];
                }
                
            }
            
            if ([object isEqualToString:@"TaskList"]) {
                NSArray *tasks = [self.managedObjectContext fetchObjectsForEntityName:@"Task"
                                                                  predicateWithFormat:[NSString stringWithFormat:@"task_list_id == %@", value]];
                
                for (Task *task in tasks) {
                    [(TaskList*)managedObject addTaskObject:task];
                }
                
            }
        }
        
        if ([attribute isEqualToString:@"updated_at"]) {
            NSDate *dateFromString = [[NSDate alloc] init];
            dateFromString = [self.dateFormatter dateFromString:value];
            [managedObject setValue:dateFromString forKey:attribute];
            
        } else {
            [managedObject setValue:value forKey:attribute];
        }
    }
    
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Error: %@", [error localizedDescription]);
    } else {
        NSLog(@"Saved: %@", managedObject);
    }
}


- (void)saveUser:(NSDictionary *)dictionary
{
    NSManagedObject *managedObject = [NSEntityDescription
                                      insertNewObjectForEntityForName:@"User"
                                      inManagedObjectContext:self.managedObjectContext];
    
    NSDictionary *attributes = [[managedObject entity] attributesByName];
    
    for (NSString *attribute in attributes) {
        
        id value = [dictionary objectForKey:attribute];
        
        if (value == nil) {
            continue;
            
        }
        
        if ([attribute isEqualToString:@"updated_at"]) {
            NSDate *dateFromString = [[NSDate alloc] init];
            dateFromString = [self.dateFormatter dateFromString:value];
            [managedObject setValue:dateFromString forKey:attribute];
            
        } else {
            [managedObject setValue:value forKey:attribute];
        }
        
    }
    
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Error: %@", [error localizedDescription]);
    } else {
        NSLog(@"Saved: %@", managedObject);
    }
    
}


- (User *)getUser
{
    return [[self.managedObjectContext fetchObjectsForEntityName:@"User"] objectAtIndex:0];
}


- (BOOL)clearData
{
    [self.managedObjectContext lock];
    [self.managedObjectContext reset];
    NSPersistentStore *store = [[self.persistentStoreCoordinator persistentStores] lastObject];
    BOOL success = NO;
	
    if (store) {
        NSURL *storeUrl = store.URL;
        NSError *error;
		
        if ([self.persistentStoreCoordinator removePersistentStore:store error:&error]) {
            self.persistentStoreCoordinator = nil;
            self.managedObjectContext = nil;
			
            if (![[NSFileManager defaultManager] removeItemAtPath:storeUrl.path error:&error]) {
                NSLog(@"\nresetDatastore. Error removing file of persistent store: %@",
					  [error localizedDescription]);
                success = NO;
            } else {
                //now recreate persistent store
                [self persistentStoreCoordinator];
                [[self managedObjectContext] unlock];
                success = YES;
            }
        } else {
            NSLog(@"\nresetDatastore. Error removing persistent store: %@",
				  [error localizedDescription]);
            success = NO;
        }
        return success;
    } else {
        NSLog(@"\nresetDatastore. Could not find the persistent store");
        return success;
    }
}

#pragma mark - Core Data stack

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}


- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}


- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"teambox" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}


- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"teambox.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
