//
//  TBAppController.m
//  teambox
//
//  Created by Ivan Bella López on 6/3/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import "TBAppController.h"

@implementation TBAppController

+ (TBAppController *)sharedClient{
    static TBAppController *sharedClient = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedClient = [[self alloc] init];
	});
	return sharedClient;
}


- (id)init
{
    if (self = [super init]) {
        //init
       self.defaults = [NSUserDefaults standardUserDefaults];
    }
    
    return self;
}



@end
