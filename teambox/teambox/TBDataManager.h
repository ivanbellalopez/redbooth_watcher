//
//  TBDataManager.h
//  teambox
//
//  Created by Ivan Bella López on 6/6/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import <Foundation/Foundation.h>

@class User;

@interface TBDataManager : NSObject


+ (TBDataManager *) sharedManager;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)save:(NSString *)object withDictionary:(NSDictionary *)dictionary;
- (void)saveUser:(NSDictionary *)dictionary;
- (User *)getUser;
- (BOOL)clearData;

@end
