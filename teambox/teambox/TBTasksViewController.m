//
//  TBTasksViewController.m
//  teambox
//
//  Created by Ivan Bella López on 6/9/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import "TBTasksViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "TBNetwork.h"
#import "TaskList.h"
#import "Task.h"
#import "TBDataManager.h"
#import "PrettyKit.h"
#import "TBConstants.h"


@interface TBTasksViewController ()

@property (nonatomic, strong) NSString *projectId;
@property (nonatomic, strong) NSString *projectTitle;

@end

@implementation TBTasksViewController

- (id) initWithProject:(Project *)project
{
    if (self = [super init]) {
        self.projectId = [project.id stringValue];
        self.projectTitle = project.name;
    }
    
    return  self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    self.tableView = [[UITableView alloc] initWithFrame:(CGRect){{kTB_Padding, kTB_Padding}, {self.view.frame.size.width - 2 * kTB_Padding, self.view.frame.size.height - 2 * kTB_Padding}} style:UITableViewStylePlain];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.layer.cornerRadius = kTB_CornerRadius;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.view addSubview:self.tableView];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSError *error;
	if (![self.getTasksFetchedResultsController performFetch:&error]) {
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		abort();
	} else if ([[self.getTasksFetchedResultsController fetchedObjects] count] > 0) {
        [self.tableView reloadData];
		
	} else {
        [[TBNetwork sharedClient] getUserTasks];
        [TBNetwork sharedClient].successBlock = ^{
            [[TBNetwork sharedClient] getUserProjectsTasksLists:self.projectId];
            [TBNetwork sharedClient].successBlock = ^{
                if ([self.getTasksFetchedResultsController performFetch:nil]) {
                    [self.tableView reloadData];
                }
            };
            
            [TBNetwork sharedClient].failureBlock = ^{
                UIAlertView *aV = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Something went wrong." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                
                [aV show];
            };
        };
    }
    
    self.title = self.projectTitle;

}


#pragma mark - TableView data source

- (void)fetchedResultsController:(NSFetchedResultsController *)aFetchedResultsController configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    TaskList *taskListInfo = [[self.getTasksFetchedResultsController fetchedObjects] objectAtIndex:indexPath.section];
    if (taskListInfo.task.count == 0) {
        cell.textLabel.text = @"Oops! No task here...";
    } else {
        TaskList *taskListInfo = [[aFetchedResultsController fetchedObjects] objectAtIndex:indexPath.section];
        Task *task = [[taskListInfo.task allObjects] objectAtIndex:indexPath.row];
        cell.textLabel.text = task.name;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int numOfRows = 0;
    
    TaskList *taskListInfo = [[self.getTasksFetchedResultsController fetchedObjects] objectAtIndex:section];
    numOfRows = taskListInfo.task.count;
    
    if (numOfRows == 0) {
        numOfRows = 1;
    }
    
    return numOfRows;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(kTB_Padding_small, kTB_Padding_small, self.tableView.frame.size.width - 2 * kTB_Padding_small, 35.0)];
    headerLabel.font = kTB_Font_Title;
    headerLabel.backgroundColor = kTB_Color_DarkBlue;
    headerLabel.textAlignment = NSTextAlignmentCenter;
    
    
    TaskList *taskListInfo = [[self.getTasksFetchedResultsController fetchedObjects] objectAtIndex:section];
    headerLabel.text = taskListInfo.name;
    return headerLabel;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellTasksIdentifier = @"CellProjects";
    PrettyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellTasksIdentifier];
    if (cell == nil) {
        cell = [[PrettyTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellTasksIdentifier];
        cell.tableViewBackgroundColor = tableView.backgroundColor;
        cell.gradientStartColor = kTB_Color_LightGray;
        cell.gradientEndColor = kTB_Color_DarkGray;
    }
    [cell prepareForTableView:tableView indexPath:indexPath];
    cell.textLabel.font = kTB_Font_CellText;
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    
    [self fetchedResultsController:self.getTasksFetchedResultsController configureCell:cell atIndexPath:indexPath];
    
    return cell;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.getTasksFetchedResultsController sections] count];
}


#pragma mark - Fetch delegate

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    UITableView *tableView;
    [tableView beginUpdates];
    [tableView endUpdates];
}

- (NSFetchedResultsController *)getTasksFetchedResultsController
{
    if (_getTasksFetchedResultsController != nil) {
        return _getTasksFetchedResultsController;
    }
    
    [NSFetchedResultsController deleteCacheWithName:@"TasksCache"];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"TaskList" inManagedObjectContext: [TBDataManager sharedManager].managedObjectContext]];
    [request setFetchLimit:0];
    [request setFetchBatchSize:20];
    [request setIncludesPropertyValues:NO];
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    [request setSortDescriptors:@[descriptor]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(project_id == %@)", self.projectId];
    [request setPredicate:predicate];
    
    
    _getTasksFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:[TBDataManager sharedManager].managedObjectContext sectionNameKeyPath:@"id" cacheName:@"TasksCache"];
    _getTasksFetchedResultsController.delegate = self;
    return _getTasksFetchedResultsController;
}

@end
