//
//  Organization.m
//  teambox
//
//  Created by Ivan Bella López on 6/6/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import "Organization.h"
#import "Project.h"


@implementation Organization

@dynamic id;
@dynamic name;
@dynamic project;

@end
