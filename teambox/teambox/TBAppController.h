//
//  TBAppController.h
//  teambox
//
//  Created by Ivan Bella López on 6/3/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TBAppController : NSObject

@property (nonatomic, strong) NSUserDefaults *defaults;
@property (nonatomic, assign) BOOL isLoggedIn;


+ (TBAppController *)sharedClient;




@end
