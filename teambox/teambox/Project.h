//
//  Project.h
//  teambox
//
//  Created by Ivan Bella López on 6/6/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Organization, TaskList;

@interface Project : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * organization_id;
@property (nonatomic, retain) NSSet *task_list;
@property (nonatomic, retain) Organization *organization;
@end

@interface Project (CoreDataGeneratedAccessors)

- (void)addTask_listObject:(TaskList *)value;
- (void)removeTask_listObject:(TaskList *)value;
- (void)addTask_list:(NSSet *)values;
- (void)removeTask_list:(NSSet *)values;

@end
