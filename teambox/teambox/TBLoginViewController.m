//
//  TBViewController.m
//  teambox
//
//  Created by Ivan Bella López on 5/31/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import "TBLoginViewController.h"
#import "TBOauthWebViewController.h"
#import "TBAppController.h"
#import "TBConstants.h"
#import "TBNetwork.h"
#import "TBProjectsViewController.h"
#import "UIView+TBAdditions.h"
#import "BButton.h"

@interface TBLoginViewController ()

@property (nonatomic, strong) BButton *loginButton;

@end

@implementation TBLoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;
    self.title = @"Teambox";
    
    UIImage *logoImage = [UIImage imageNamed:@"teambox.png"];
    UIImageView *logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 130.0, roundf(logoImage.size.width/2), roundf(logoImage.size.height/2))];
    logoImageView.image = logoImage;
    [logoImageView tbCenterHorizontallyInView:self.view];

    
    self.loginButton = [[BButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 160.0, 40.0) type:BButtonTypePrimary];
    self.loginButton.titleLabel.font = kTB_Font_Button;
    [self.loginButton setTitle:@"Login" forState:UIControlStateNormal];
    [self.loginButton addTarget:self action:@selector(_loginAction:) forControlEvents:UIControlEventTouchUpInside];
    self.loginButton.enabled = NO;
    [self.loginButton tbCenterInView:self.view];
    
    [self.view addSubview:logoImageView];
    [self.view addSubview:self.loginButton];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([self _isOAuthRequired]) {
        self.loginButton.enabled = YES;
        [TBAppController sharedClient].isLoggedIn = NO;
    } else {
        [[TBNetwork sharedClient] getUserInfo];
        [self showSpinner];
        [TBNetwork sharedClient].successBlock = ^ {
            [TBAppController sharedClient].isLoggedIn = YES;
            [self hideSpinner];
            TBProjectsViewController *projectsViewController = [[TBProjectsViewController alloc] init];
            [self.navigationController pushViewController:projectsViewController animated:YES];
        };
    }
}


#pragma mark - OAuth

- (BOOL)_isOAuthRequired
{
    NSString *token = [[TBAppController sharedClient].defaults valueForKey:kTB_OAuth_Code];
    if (token && [token length] > 0) {
        return NO;
    }
    
    return YES;
}


#pragma mark - Actions

- (void)_loginAction:(id)sender
{
    TBOauthWebViewController *vc = [[TBOauthWebViewController alloc] init];
    [self.navigationController presentViewController:vc animated:YES completion:nil];
}

@end
