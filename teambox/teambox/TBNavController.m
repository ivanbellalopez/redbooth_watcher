//
//  TBNavController.m
//  teambox
//
//  Created by Ivan Bella López on 6/9/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import "TBNavController.h"
#import "PrettyKit.h"

@interface TBNavController ()

@end

@implementation TBNavController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (id)initWithRootViewController:(UIViewController *)rootViewController
{
    self = [super initWithRootViewController:rootViewController];
    if (self) {
        [self setValue:[[PrettyNavigationBar alloc] init] forKeyPath:@"navigationBar"];
    }
    
    return self;
}

@end
