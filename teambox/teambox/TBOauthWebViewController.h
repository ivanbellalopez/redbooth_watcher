//
//  TBOauthWebViewController.h
//  teambox
//
//  Created by Ivan Bella López on 5/31/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TBOauthWebViewController : UIViewController <UIWebViewDelegate>

@end
