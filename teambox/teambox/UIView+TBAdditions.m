//
//  TBOauthWebViewController.m
//  teambox
//
//  Created by Ivan Bella López on 5/31/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import "UIView+TBAdditions.h"
#import <QuartzCore/QuartzCore.h>


@implementation UIView (TBAdditions)

- (void)tbSetWidth:(CGFloat)width
{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}


- (void)tbMoveToX:(CGFloat)originX
{
    CGRect frame = self.frame;
    frame.origin.x = originX;
    self.frame = frame;
}

- (void)tbMoveToY:(CGFloat)originY
{
    CGRect frame = self.frame;
    frame.origin.y = originY;
    self.frame = frame;
}


- (void)tbMoveToYByOffset:(CGFloat)offsetY
{
    CGRect frame = self.frame;
    frame.origin.y += offsetY;
    self.frame = frame;
}


- (void)tbMoveToXByOffset:(CGFloat)offsetX
{
    CGRect frame = self.frame;
    frame.origin.x += offsetX;
    self.frame = frame;
}


- (void)tbCenterInView:(UIView *)view
{
	self.frame = (CGRect) {
		{ round((view.frame.size.width - self.frame.size.width) / 2),
			round((view.frame.size.height - self.frame.size.height) / 2),
		},
		self.frame.size
	};
}


- (void)tbCenterVerticallyInView:(UIView *)view
{
	self.frame = (CGRect) {
		{ self.frame.origin.x,
			round((view.frame.size.height - self.frame.size.height) / 2),
		},
		self.frame.size
	};
}


- (void)tbCenterHorizontallyInView:(UIView *)view
{
	self.frame = (CGRect) {
		{ round((view.frame.size.width - self.frame.size.width) / 2),
			self.frame.origin.y,
		},
		self.frame.size
	};
}

@end
