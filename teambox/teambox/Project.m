//
//  Project.m
//  teambox
//
//  Created by Ivan Bella López on 6/6/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import "Project.h"
#import "Organization.h"
#import "TaskList.h"


@implementation Project

@dynamic id;
@dynamic name;
@dynamic organization_id;
@dynamic task_list;
@dynamic organization;

@end
