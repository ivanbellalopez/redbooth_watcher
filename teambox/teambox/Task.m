//
//  Task.m
//  teambox
//
//  Created by Ivan Bella López on 6/9/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import "Task.h"
#import "TaskList.h"


@implementation Task

@dynamic id;
@dynamic name;
@dynamic updated_at;
@dynamic user_id;
@dynamic task_list_id;
@dynamic task_list;

@end
