//
//  TBViewController.m
//  teambox
//
//  Created by Ivan Bella López on 6/4/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import "TBViewController.h"
#import "TBAppController.h"
#import "TBDataManager.h"
#import "TBLoginViewController.h"
#import "PrettyKit.h"
#import "BButton.h"
#import "UIView+TBAdditions.h"
#import "TBConstants.h"


@interface TBViewController ()

@property (nonatomic, strong) BButton *logoutButton;

@end

@implementation TBViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    backgroundImageView.image = [UIImage imageNamed:@"background.jpg"];
    [self.view addSubview:backgroundImageView];
    
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                UITextAttributeTextColor: [UIColor whiteColor],
                          UITextAttributeTextShadowColor: [UIColor darkGrayColor],
                         UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0.0f, -1.0f)],
                                     UITextAttributeFont: kTB_Font_Title
     }];
    
    
    if ([TBAppController sharedClient].isLoggedIn) {
        
        self.logoutButton = [[BButton alloc] initWithFrame:CGRectMake(0, 0, 60.0, 30.0) color:[UIColor redColor]];
        self.logoutButton.titleLabel.font = kTB_Font_Button_Small;
        [self.logoutButton addTarget:self action:@selector(_logout:) forControlEvents:UIControlEventTouchUpInside];
        [self.logoutButton setTitle:@"Logout" forState:UIControlStateNormal];
        [self.logoutButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.logoutButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        
        UIBarButtonItem *barButtonCancel = [[UIBarButtonItem alloc] initWithCustomView:self.logoutButton];
        self.navigationItem.rightBarButtonItem = barButtonCancel;
        
    }
    
    [self _setUpSpinner];
    [self _setUpNavBar];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([self isKindOfClass:[TBLoginViewController class]]) {
        self.navigationController.navigationBarHidden = YES;
    } else {
        self.navigationController.navigationBarHidden = NO;
    }
}


- (void)_setUpSpinner
{
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.spinner.frame = CGRectMake(0, 0, 70.0, 70.0);
    self.spinner.layer.backgroundColor = [UIColor darkGrayColor].CGColor;
    self.spinner.layer.cornerRadius = 5.0;
    [self.spinner tbCenterInView:self.view];
    self.spinner.alpha = 0.0;
    [self.spinner startAnimating];
    
    [self.view addSubview:self.spinner];
}


- (void)showSpinner
{
    [self.view bringSubviewToFront:self.spinner];
    [UIView animateWithDuration:0.25 animations:^{
        self.spinner.alpha = 1.0;
    }];
}


- (void)hideSpinner
{
    [UIView animateWithDuration:0.25 animations:^{
        self.spinner.alpha = 0.0;
    }];
}


- (void)_setUpNavBar
{
    PrettyNavigationBar *navBar = (PrettyNavigationBar *)self.navigationController.navigationBar;
    navBar.topLineColor = [UIColor colorWithHex:0x84B7D5];
    navBar.gradientStartColor = [UIColor colorWithHex:0x53A4DE];
    navBar.gradientEndColor = [UIColor colorWithHex:0x297CB7];
    navBar.bottomLineColor = [UIColor colorWithHex:0x186399];
    navBar.tintColor = [UIColor colorWithHex:0x3D89BF];
    navBar.roundedCornerRadius = 3;
}


- (void)_logout:(id)sender
{
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
    [TBAppController sharedClient].isLoggedIn = NO;
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
	[[TBDataManager sharedManager] clearData];
}

@end
