//
//  TaskList.m
//  teambox
//
//  Created by Ivan Bella López on 6/6/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import "TaskList.h"
#import "Project.h"
#import "Task.h"


@implementation TaskList

@dynamic id;
@dynamic name;
@dynamic user_id;
@dynamic updated_at;
@dynamic task;
@dynamic project;

@end
