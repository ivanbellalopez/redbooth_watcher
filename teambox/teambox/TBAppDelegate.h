//
//  TBAppDelegate.h
//  teambox
//
//  Created by Ivan Bella López on 5/31/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TBNavController.h"

@class TBLoginViewController;

@interface TBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) TBNavController *navigationController;
@property (strong, nonatomic) TBLoginViewController *viewController;

@end
