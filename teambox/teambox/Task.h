//
//  Task.h
//  teambox
//
//  Created by Ivan Bella López on 6/9/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TaskList;

@interface Task : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSDate * updated_at;
@property (nonatomic, retain) NSNumber * user_id;
@property (nonatomic, retain) NSNumber * task_list_id;
@property (nonatomic, retain) TaskList *task_list;

@end
