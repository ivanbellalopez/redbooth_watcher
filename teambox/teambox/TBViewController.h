//
//  TBViewController.h
//  teambox
//
//  Created by Ivan Bella López on 6/4/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TBViewController : UIViewController

@property (nonatomic, strong) UIActivityIndicatorView *spinner;

- (void)showSpinner;
- (void)hideSpinner;

@end
