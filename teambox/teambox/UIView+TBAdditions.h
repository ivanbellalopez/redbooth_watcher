//
//  TBOauthWebViewController.m
//  teambox
//
//  Created by Ivan Bella López on 5/31/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (TBAdditions)

- (void)tbSetWidth:(CGFloat)width;

- (void)tbMoveToX:(CGFloat)originX;
- (void)tbMoveToY:(CGFloat)originY;

- (void)tbMoveToYByOffset:(CGFloat)offsetY;
- (void)tbMoveToXByOffset:(CGFloat)offsetX;


- (void)tbCenterInView:(UIView *)view;
- (void)tbCenterVerticallyInView:(UIView *)view;
- (void)tbCenterHorizontallyInView:(UIView *)view;


@end
