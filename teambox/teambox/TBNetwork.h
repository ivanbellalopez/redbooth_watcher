//
//  TBNetwork.h
//  teambox
//
//  Created by Ivan Bella López on 5/31/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import "AFHTTPClient.h"
#import <Foundation/Foundation.h>

typedef void (^TBNetworkTokenBlock)(void);


@interface TBNetwork : AFHTTPClient


@property (nonatomic, strong) TBNetworkTokenBlock successBlock;
@property (nonatomic, strong) TBNetworkTokenBlock failureBlock;


+ (TBNetwork*)sharedClient;

//oauth
- (void)getAccessToken;


//users
- (void)getUserInfo;

//info
- (void)getUserOrganizations;
- (void)getUserProjects;
- (void)getUserProjectsTasksLists:(NSString *)projectId;
- (void)getUserTasks;
- (void)getUserTaskInfo:(NSString *)taskId;

@end
