//
//  TBConstants.h
//  teambox
//
//  Created by Ivan Bella López on 5/31/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//


#define TBAppKey @"CNqZB30HmJeQUHO9oMK5jQwneN0XKtQ9QLDREi3I"
#define TBAppSecret @"YI2f94pCvSIctHaB7M90r4Tq2JHYRoo8PaoIfpUb"

//NSUserDefaults keys
#define kTB_OAuth_Code @"kTB_OAuth_Code"
#define kTB_Return_Code @"kTB_Return_Cod"

//Fonts

#define kTB_Font_Button [UIFont fontWithName:@"Avenir-Medium" size:15.0]
#define kTB_Font_Title [UIFont fontWithName:@"Avenir-Heavy" size:17.0]
#define kTB_Font_Button_Small [UIFont fontWithName:@"Avenir-Medium" size:12.0]

//Table

#define kTB_Padding 10.0
#define kTB_Padding_small 5.0


#define kTB_CornerRadius 5.0
#define kTB_Color_DarkBlue [UIColor colorWithHex:0x297CB7]
#define kTB_Color_LightGray [UIColor colorWithHex:0xEEEEEE]
#define kTB_Color_DarkGray [UIColor colorWithHex:0xCCCCCC]

#define kTB_Font_CellText [UIFont fontWithName:@"Avenir-Book" size:12.0]


//Network

#define kTB_Base_URL @"https://teambox.com"
#define kTB_Base_URL_API @"https://teambox.com/api/2"