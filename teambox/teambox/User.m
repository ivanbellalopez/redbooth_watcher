//
//  User.m
//  teambox
//
//  Created by Ivan Bella López on 6/10/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import "User.h"


@implementation User

@dynamic avatar_url;
@dynamic updated_at;
@dynamic user_id;
@dynamic username;

@end
