//
//  TBNetwork.m
//  teambox
//
//  Created by Ivan Bella López on 5/31/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import "TBNetwork.h"
#import "AFJSONRequestOperation.h"
#import "TBConstants.h"
#import "AFJSONRequestOperation.h"
#import "TBAppController.h"
#import "TBDataManager.h"

@interface TBNetwork ()

@property (nonatomic, strong) NSString *token;

@end

@implementation TBNetwork

+ (TBNetwork *)sharedClient {
	static TBNetwork *sharedClient = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.teambox.com"]];
	});
	return sharedClient;
}


#pragma mark - OAuth

- (void)getAccessToken
{
    NSString *path = [NSString stringWithFormat:@"%@/oauth/token?client_id=%@&client_secret=%@&code=%@&grant_type=authorization_code&redirect_uri=http://www.3lokoj.com", kTB_Base_URL, TBAppKey,TBAppSecret,[[TBAppController sharedClient].defaults valueForKey:kTB_Return_Code]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:path]];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        
        [[TBAppController sharedClient].defaults setObject:[JSON objectForKey:@"access_token"] forKey:kTB_OAuth_Code];
        self.token = [NSString stringWithString:[JSON objectForKey:@"access_token"]];
        [self _sucessBlock];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        UIAlertView *aV = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [aV show];
        [self _failureBlock];
        
    }];
    
    [operation start];
}


#pragma mark - User

- (void)getUserInfo
{
    self.successBlock = nil;
    self.failureBlock = nil;
    
    self.token = [NSString stringWithString:[[TBAppController sharedClient].defaults valueForKey:kTB_OAuth_Code]];
    
    NSString *path = [NSString stringWithFormat:@"%@/users/?access_token=%@", kTB_Base_URL_API, self.token];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:path]];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        
        [[TBDataManager sharedManager] saveUser:JSON[0]];
        [self _sucessBlock];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        UIAlertView *aV = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [aV show];
    }];
    
    [operation start];
}


#pragma mark - Info

- (void) getUserOrganizations
{
    self.successBlock = nil;
    self.failureBlock = nil;
    
    NSString *path = [NSString stringWithFormat:@"%@/organizations/?access_token=%@", kTB_Base_URL_API, self.token];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:path]];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        for (NSDictionary *aOrganization in JSON) {
            
            [[TBDataManager sharedManager] save:@"Organization" withDictionary:aOrganization];
        }
        
        [self _sucessBlock];
        
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        UIAlertView *aV = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [aV show];
    }];
    
    [operation start];
}


- (void)getUserProjects
{
    self.successBlock = nil;
    self.failureBlock = nil;
    
    NSString *path = [NSString stringWithFormat:@"%@/projects/?access_token=%@", kTB_Base_URL_API, self.token];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:path]];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        for (NSDictionary *aProject in JSON) {
            
            [[TBDataManager sharedManager] save:@"Project" withDictionary:aProject];
        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        UIAlertView *aV = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [aV show];
    }];
    
    [operation start];
}


- (void)getUserProjectsTasksLists:(NSString *)projectId
{
    self.successBlock = nil;
    self.failureBlock = nil;
    
    NSString *path = [NSString stringWithFormat:@"%@/projects/%@/task_lists/?access_token=%@", kTB_Base_URL_API, projectId , self.token];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:path]];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        for (NSDictionary *aProject in JSON) {
            
            [[TBDataManager sharedManager] save:@"TaskList" withDictionary:aProject];
        }
        
        [self _sucessBlock];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        UIAlertView *aV = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [aV show];
    }];
    
    [operation start];
}


- (void)getUserTasks
{
    self.successBlock = nil;
    self.failureBlock = nil;
    
    NSString *path = [NSString stringWithFormat:@"%@/tasks/?access_token=%@", kTB_Base_URL_API, self.token];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:path]];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        for (NSDictionary *aProject in JSON) {
            
            [[TBDataManager sharedManager] save:@"Task" withDictionary:aProject];
        }
        
        [self _sucessBlock];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        UIAlertView *aV = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [aV show];
    }];
    
    [operation start];
}


- (void)getUserTaskInfo:(NSString *)taskId
{
    self.successBlock = nil;
    self.failureBlock = nil;
    
    NSString *path = [NSString stringWithFormat:@"%@/tasks/?access_token=%@", kTB_Base_URL_API, self.token];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:path]];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        for (NSDictionary *aProject in JSON) {
            
            [[TBDataManager sharedManager] save:@"Task" withDictionary:aProject];
        }
        
        [self _sucessBlock];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        UIAlertView *aV = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [aV show];
    }];
    
    [operation start];
}


#pragma mark - Blocks

- (void)_sucessBlock
{
    if (self.successBlock) {
        self.successBlock();
    }
}


- (void)_failureBlock
{
    if (self.failureBlock) {
        self.failureBlock();
    }
}

@end
