//
//  TBProjectsViewController.m
//  teambox
//
//  Created by Ivan Bella López on 6/3/13.
//  Copyright (c) 2013 3Lokoj. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "TBProjectsViewController.h"
#import "TBNetwork.h"
#import "Organization.h"
#import "Project.h"
#import "TBDataManager.h"
#import "TBTasksViewController.h"
#import "PrettyKit.h"
#import "AFNetworking.h"
#import "TBDataManager.h"
#import "User.h"
#import "TBConstants.h"

#define _k_img_size 40.0
#define _k_view_height 60.0

@interface TBProjectsViewController ()

@property (nonatomic, strong) UIView *profileView;

@end


@implementation TBProjectsViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    [self _buildProfileView];
    
    self.tableView = [[UITableView alloc] initWithFrame:(CGRect){{kTB_Padding, self.profileView.frame.size.height + 2 * kTB_Padding}, {self.view.frame.size.width - 2 * kTB_Padding, self.view.frame.size.height - 2 * kTB_Padding}} style:UITableViewStylePlain];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.layer.cornerRadius = kTB_CornerRadius;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.view addSubview:self.tableView];
    
    self.navigationItem.hidesBackButton = YES;
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSError *error;
    if (![self.getProjectsFetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    } else if ([[self.getProjectsFetchedResultsController fetchedObjects] count] > 0) {
        [self.tableView reloadData];
        
    } else {
        [self showSpinner];
        [[TBNetwork sharedClient] getUserProjects];
        [[TBNetwork sharedClient] getUserOrganizations];
        [TBNetwork sharedClient].successBlock = ^{
            if ([self.getProjectsFetchedResultsController performFetch:nil]) {
                [self.tableView reloadData];
                [self hideSpinner];
            }
        };
        
        [TBNetwork sharedClient].failureBlock = ^{
            [self hideSpinner];
            UIAlertView *aV = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Something went wrong." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [aV show];
        };
    }
    
    self.title = @"Projects";
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)_buildProfileView
{
    self.profileView = [[UIView alloc] initWithFrame:CGRectMake(kTB_Padding, kTB_Padding, self.view.frame.size.width - 2 * kTB_Padding, _k_view_height)];
    self.profileView.backgroundColor = kTB_Color_DarkBlue;
    self.profileView.layer.cornerRadius = kTB_CornerRadius;
    [self.view addSubview:self.profileView];
    
    UIImageView *profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(kTB_Padding, kTB_Padding, _k_img_size, _k_img_size)];
    profileImageView.backgroundColor = [UIColor grayColor];
    
    
    UILabel *userNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(kTB_Padding + profileImageView.frame.size.width + profileImageView.frame.origin.x, kTB_Padding, self.profileView.frame.size.width - _k_img_size - 3 * kTB_Padding, _k_img_size)];
    userNameLabel.backgroundColor = [UIColor clearColor];
    userNameLabel.font = kTB_Font_Button;
    userNameLabel.textAlignment = NSTextAlignmentLeft;
    userNameLabel.textColor = [UIColor whiteColor];
    userNameLabel.numberOfLines = 0;
    
    User *user = [[TBDataManager sharedManager] getUser];
    NSString *userThumb = user.avatar_url;
    
    [profileImageView setImageWithURL:[NSURL URLWithString:userThumb]];
    [self.profileView addSubview:profileImageView];
    userNameLabel.text = [NSString stringWithFormat:@"Welcome %@!", user.username];
    
    [self.profileView addSubview:userNameLabel];
}


#pragma mark - TableView data source

- (void)fetchedResultsController:(NSFetchedResultsController *)aFetchedResultsController configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Organization *organizationInfo = [[aFetchedResultsController fetchedObjects] objectAtIndex:indexPath.section];
    Project *project = [[organizationInfo.project allObjects] objectAtIndex:indexPath.row];
    
    cell.textLabel.text = project.name;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    Organization *organizationInfo = [[self.getProjectsFetchedResultsController fetchedObjects] objectAtIndex:section];
    return organizationInfo.project.count;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(5.0, 5.0, self.tableView.frame.size.width - 10.0, 35.0)];
    headerLabel.font = kTB_Font_Title;
    headerLabel.backgroundColor = kTB_Color_DarkBlue;
    headerLabel.textAlignment = NSTextAlignmentCenter;
    
    Organization *organizationInfo = [[self.getProjectsFetchedResultsController fetchedObjects] objectAtIndex:section];
    headerLabel.text = organizationInfo.name;
    return headerLabel;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellProjectsIdentifier = @"CellProjects";
    
    PrettyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellProjectsIdentifier];
    if (cell == nil) {
        cell = [[PrettyTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellProjectsIdentifier];
        cell.tableViewBackgroundColor = tableView.backgroundColor;
        cell.gradientStartColor = kTB_Color_LightGray;
        cell.gradientEndColor = kTB_Color_DarkGray;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    [cell prepareForTableView:tableView indexPath:indexPath];
    cell.textLabel.font = kTB_Font_CellText;
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    [self fetchedResultsController:self.getProjectsFetchedResultsController configureCell:cell atIndexPath:indexPath];
    
    return cell;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.getProjectsFetchedResultsController sections] count];
}


#pragma mark - TableView delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Organization *organizationInfo = [[self.getProjectsFetchedResultsController fetchedObjects] objectAtIndex:indexPath.section];
    Project *project = [[organizationInfo.project allObjects] objectAtIndex:indexPath.row];
    
    TBTasksViewController *tasksVC = [[TBTasksViewController alloc] initWithProject:project];
    
    [self.navigationController pushViewController:tasksVC animated:YES];
    
}


#pragma mark - Fetch delegate

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self showSpinner];
    
    UITableView *tableView;
    [tableView beginUpdates];
    [tableView endUpdates];
    [self hideSpinner];
    
}


#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)getProjectsFetchedResultsController {
    if (_getProjectsFetchedResultsController != nil) {
        return _getProjectsFetchedResultsController;
    }
    
    [NSFetchedResultsController deleteCacheWithName:@"OrganizationCache"];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"Organization" inManagedObjectContext: [TBDataManager sharedManager].managedObjectContext]];
    [request setFetchLimit:0];
    [request setFetchBatchSize:20];
    [request setIncludesPropertyValues:NO];
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    [request setSortDescriptors:@[descriptor]];
    
    _getProjectsFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:[TBDataManager sharedManager].managedObjectContext sectionNameKeyPath:@"id" cacheName:@"OrganizationCache"];
    _getProjectsFetchedResultsController.delegate = self;
    
    return _getProjectsFetchedResultsController;
}

@end
